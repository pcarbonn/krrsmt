module Main where

  import Language.SMTLib2
  import Language.SMTLib2.Pipe (SMTPipe(..), createPipe)
  import qualified Language.SMTLib2.Internals.Expression as E
  import qualified Language.SMTLib2.Internals.Backend as B
  import qualified Language.SMTLib2.Internals.Type.List as List
  import Data.Text

  {-
    parse to AST
    in monad:
      build maps (varName -> declared var, atom -> declared atom)
      generate assertions
      find model
      get values of models
  -}

  query :: (Backend b)
    => ( List Repr tps
       , (List (Expr b) tps -> SMT b ()))
    -> SMT b (Maybe (List Value tps))
  query (tps, f) = do
    args <- List.mapM declareVar tps
    f args
    res <- checkSat
    case res of
      Sat -> do
        vals <- List.mapM getValue args
        return $ Just vals
      _ -> return Nothing

  source :: (List Repr [RealType, RealType]
       , List (Expr SMTPipe) [RealType,RealType] -> SMT SMTPipe ())
  source = ( (real ::: real ::: Nil)
      , (\(x ::: y ::: Nil) -> do
              x .+. y .==. creal 1.0 >>= assert)
      )

  main = withBackend (createPipe "z3" ["-smt2","-in"]) (query source) >>= print
